package com.job.queue.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.job.queue.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
	
	@Query("SELECT p FROM Product p "
			+ "JOIN ProductDiscount pd "
			+ "ON p.id = pd.product.id "
			+ "WHERE p.hasDiscount = 0")
	public List<Product> findProductsWithDiscount();
	
	public Product save(Product product);

}
