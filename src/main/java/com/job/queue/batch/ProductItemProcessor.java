package com.job.queue.batch;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.job.queue.model.Product;

public class ProductItemProcessor implements ItemProcessor<List<Product>, List<Product>> {
	private static final Logger log = LoggerFactory.getLogger(ProductItemProcessor.class);

	private static final Boolean HAS_DISCOUNT = true;
	
    @Override
    public List<Product> process(List<Product> products) throws Exception {
    	String mensagemInicioProcessamento = "Início do processamento.";
    	log.info(mensagemInicioProcessamento);
    	products.forEach(p -> p.setHasDiscount(HAS_DISCOUNT));
    	String mensagemFinalProcessamento = "Fim do processamento.";
    	log.info(mensagemFinalProcessamento);
        return products;
    }
}
