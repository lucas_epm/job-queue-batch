create table discount_notification (
	id bigint not null auto_increment,
    notification_date date not null,
    product_id bigint,
    
    primary key (id),
    foreign key (product_id) references product(id)
);