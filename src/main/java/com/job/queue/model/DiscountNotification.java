package com.job.queue.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "discount_notification")
public class DiscountNotification {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(
			name = "product_id",
		    foreignKey = @ForeignKey(name = "id", value = ConstraintMode.NO_CONSTRAINT))
	private Product product;
	
	@Column(name = "notification_date")
	private Date notificationDate;

}
