DELIMITER //
create procedure populate_discounts()
begin
	declare product_id bigint;
    declare discount_value varchar(20);
	select id into product_id from product where name = 'smartphone motorola moto g8'
	and size = 'small' and color = 'blue';
	
	set discount_value := '100,00';
	insert into discount(discount, product_id) values (discount_value, product_id);
	commit;
end
//