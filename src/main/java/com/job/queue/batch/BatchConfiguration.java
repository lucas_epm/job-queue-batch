package com.job.queue.batch;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.job.queue.model.Product;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

	private final JobBuilderFactory jobBuilderFactory;

    private final StepBuilderFactory stepBuilderFactory;
    
	public BatchConfiguration(JobBuilderFactory jobBuilderFactory,
            StepBuilderFactory stepBuilderFactory,
            DataSource dataSource) {
		this.jobBuilderFactory = jobBuilderFactory;
		this.stepBuilderFactory = stepBuilderFactory;
	}
	
	@Bean
	public ProductItemReader reader() {
		return new ProductItemReader();
		
	}
	
	@Bean
    public ProductItemProcessor processor() {
        return new ProductItemProcessor();
    }
	
	@Bean
    public ProductItemWriter writer() {
        return new ProductItemWriter();
    }
	
	@Bean
    public Job importDiscountJob(JobCompletionNotificationListener listener) {
        return jobBuilderFactory.get("importDiscountJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(firstStep())
                .end()
                .build();
    }

    @Bean
    public Step firstStep() {
        return stepBuilderFactory.get("firstStep")
                .<List<Product>, List<Product>>chunk(1)
                .reader(reader())
                .processor(processor())
                .writer(writer())
                .build();
    }
}
