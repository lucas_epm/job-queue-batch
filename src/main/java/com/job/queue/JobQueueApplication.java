package com.job.queue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobQueueApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobQueueApplication.class, args);
	}

}
