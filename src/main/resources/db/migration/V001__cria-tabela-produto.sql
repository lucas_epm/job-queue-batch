create table product (
	id bigint not null auto_increment,
    name varchar(60) not null,
    size varchar(5) not null,
    color varchar(20) not null,
    price varchar(10) not null,
    
    primary key (id)
);