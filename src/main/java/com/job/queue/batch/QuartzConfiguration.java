package com.job.queue.batch;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QuartzConfiguration {
	
	@Value("${job.cron.expression}")
	private String jobCronExpression;

	@Bean
	public JobDetail quartzJobDetail() {
		return JobBuilder.newJob(BatchScheduleJob.class).storeDurably().build();
	}

	@Bean
	public Trigger jobTrigger() {
		return TriggerBuilder
				.newTrigger()
				.forJob(quartzJobDetail())
				.withSchedule(CronScheduleBuilder.cronSchedule(jobCronExpression)
						.withMisfireHandlingInstructionDoNothing())
				.build();
	}
}
