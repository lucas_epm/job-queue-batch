create table discount (
	id bigint not null auto_increment,
    discount varchar(10) not null,
    product_id bigint,
    
    primary key (id),
    foreign key (product_id) references product(id)
);