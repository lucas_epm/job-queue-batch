package com.job.queue.batch;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;

import com.job.queue.model.Product;
import com.job.queue.repository.ProductRepository;

public class ProductItemReader implements ItemReader<List<Product>> {

	private static final Logger log = LoggerFactory.getLogger(ProductItemProcessor.class);
	
	@Autowired
	ProductRepository productRepository;
	
	@Override
	public List<Product> read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		String mensagemInicioReader = "Início da leitura.";
    	log.info(mensagemInicioReader);
		List<Product> products = productRepository.findProductsWithDiscount();
		String mensagemFimReader = "Fim da leitura.";
    	log.info(mensagemFimReader);
    	
    	if (!products.isEmpty()) {
    		return products;
    	}
        return null;
	}

}
