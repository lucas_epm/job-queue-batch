package com.job.queue.batch;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.job.queue.Sender;
import com.job.queue.model.Product;
import com.job.queue.repository.ProductRepository;

public class ProductItemWriter implements ItemWriter<List<Product>>{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductItemWriter.class);
	
	@Autowired
	Sender rabbitMQSender;
	
	@Autowired
	ProductRepository productRepository;

	@Override
	public void write(List<? extends List<Product>> products) throws Exception {
		String mensagemInicioWriter = "Início da escrita.";
    	LOGGER.info(mensagemInicioWriter);
    	List<Product> productList = products.get(0);
    	for (Product product : productList) {
    		productRepository.save(product);
    		rabbitMQSender.send(product);
    		LOGGER.info("Message sent to the RabbitMQ successfully!");
		}
		String mensagemFinalProcessamento = "Fim da escrita.";
    	LOGGER.info(mensagemFinalProcessamento);
	}

}
